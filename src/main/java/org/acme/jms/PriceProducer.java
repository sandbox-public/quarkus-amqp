package org.acme.jms;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.Queue;
import javax.jms.Session;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;

/**
 * A bean producing random prices every 5 seconds and sending them to the prices JMS queue.
 */
@ApplicationScoped
public class PriceProducer implements Runnable {

    @Inject
    ConnectionFactory connectionFactory;

    private Queue destination = null;
    private JMSContext context = null;
    private JMSProducer producer = null;
    private final Random random = new Random();
    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

    void onStart(@Observes StartupEvent ev) {
        try
        {
            context = connectionFactory.createContext(Session.AUTO_ACKNOWLEDGE);
            destination = context.createQueue("fulfillment.quarkus");
            // destination = context.createQueue("fulfillment");
            producer = context.createProducer();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        scheduler.scheduleWithFixedDelay(this, 0L, 5L, TimeUnit.SECONDS);
    }


    void onStop(@Observes ShutdownEvent ev) {
        scheduler.shutdown();
    }


    @Override
    public void run() {
        System.out.println("about to send message...");
        // producer.send(context.createQueue("fulfillment.quarkus"), "{\"status\":\"completed\", \"system_id\":\"backend2\", \"identifier\":\""+Integer.toString(random.nextInt(100))+"\"}");
        producer.send(destination, "{\"status\":\"completed\", \"system_id\":\"backend2\", \"identifier\":\""+Integer.toString(random.nextInt(100))+"\"}");
        System.out.println("message sent.");
    }
}